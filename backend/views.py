from django.conf import settings
from django.contrib.auth import logout, login as auth_login
from django.contrib import auth
from users.models import User
from django.shortcuts import render, redirect, get_object_or_404
from django.views import View
from django.http import HttpResponse
from entitlements.exceptions import ValidationError as ValidError
from api.auth.authmachine_client import AuthMachineClient
from backend.services import update_user, sign_up, update_user_permissions
from talent.models import Person
from work.models import Challenge, Product


class OIDCallbackView(View):

    def get(self, request):
        client = AuthMachineClient(request)
        a_resp = client.get_authorization_response()
        token = client.get_access_token(a_resp)
        is_register = client.get_register(a_resp)
        request.session["token"] = token.to_json()
        user_info = client.get_userinfo(a_resp)
        user_email = user_info["email"]
        print(user_info, flush=True)

        # # check if current instance is not developer edition
        # try:
        #     validate_license()
        # except ValidError as e:
        #     print("Validation error", e, flush=True)
        #     return redirect(settings.FRONT_END_SERVER)

        redirect_url = settings.FRONT_END_SERVER

        if is_register:
            redirect_url += '?new=1'

        is_exists_person_by_email = Person.objects.filter(email_address=user_email).exists()
        is_exists_user_by_email = User.objects.filter(email=user_email).exists()
        if not (is_exists_person_by_email and is_exists_user_by_email):
            user = sign_up(user_info, 0)
        else:
            user = User.objects.filter(email=user_email).first()

        update_user_permissions(request, user, user_info["id"])

        auth_login(request, user)
        return redirect(redirect_url)


class OIDCallbackLogoutView(View):

    def get(self, request):
        logout(request)
        return redirect(settings.FRONT_END_SERVER)


def main(request):
    if request.GET.get('tab') == 'products':
        product_list = Product.objects.all()
        return render(request, 'products.html', locals())    
    challenge_list = Challenge.objects.all()
    return render(request, 'challanges.html', locals())


def authlogin(request):
    if request.session.has_key('err'):
        err = request.session.get('err')
        del request.session['err']

    if request.session.has_key('post'):
        post_data = request.session.get('post')
        del request.session['post']


    if request.POST:
        post_data = dict(request.POST)
        #request.session['post'] = post_data
        try:
            username = request.POST.get('username')
            password = request.POST.get('password')
            u = User.objects.get(username=username)

            cek_auth = auth.authenticate(username=u.username, password=password)
            auth.login(request, cek_auth)

            if request.GET.get('next'):
                return HttpResponseRedirect(request.GET.get('next'))
            
            return redirect('main')
        except Exception as err:
            request.session['err'] = str(err)
            return redirect('login')
    return render(request, 'login.html', locals())

def authlogout(request):
    auth.logout(request)
    return redirect('main')

def authreset(request):
    return render(request, 'reset.html', locals())

def addproducts(request):
    return render(request, 'addproducts.html', locals())

def authregister(request):
    return render(request, 'register.html', locals())

def product(request, key=None):
    if key:
        lookup = Challenge.objects.get(id=key)
    return render(request, 'product/main.html', locals())    